from django.test import TestCase, Client
from django.urls import resolve

from .views import books

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
import time



# Unit Test
class MyappUnitTest(TestCase):
    def test_myapp_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_myapp_using_landing_func(self):
        found = resolve('/')
        self.assertEqual(found.func, books)

    def test_myapp_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'books.html')



class MyappFunctionalTest(StaticLiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(MyappFunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(MyappFunctionalTest, self).tearDown()


    def test_title_element_in_project(self):
        self.browser.get(self.live_server_url + "/")

        title = self.browser.find_element_by_tag_name('h1')
        self.assertEquals(title.text, "--FIND YOUR BOOK--")
        time.sleep(10)
